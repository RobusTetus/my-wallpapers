#/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

source $SCRIPT_DIR/folders.sh

if [[ `id -u` -eq 0 ]]; then
	echo "Please do not run this script as root. The script will automatically ask you for sudo authentication when needed."
	exit 1
fi

raise_sudo()
{
	if [[ ! `id -u` -eq 0 && $SYSTEM_WIDE -eq 1 ]]; then
		sudo $@
	else
		$@
	fi
}

set_up()
{
	if [[ $SYSTEM_WIDE -eq 1 ]]; then	
		WALLPAPERS_DIRECTORY=$ROOT_WP_DIR

		if [[ ${XDG_SESSION_DESKTOP^^} == "GNOME" ]]; then
			BACKGROUND_PROPERTIES=$ROOT_BG_CFG
			echo "Detected GNOME Session. Installing background properties too..."
		fi

		echo "Installing wallpapers system-wide..."
	else
		WALLPAPERS_DIRECTORY=$USER_WP_DIR

		if [[ ${XDG_SESSION_DESKTOP^^} == "GNOME" ]]; then
			BACKGROUND_PROPERTIES=$USER_BG_CFG
			echo "Detected GNOME Session. Installing background properties too..."
		fi

		echo "Installing wallpapers to user directory..."
	fi
}

while true; do
	read -p "Do you wish to install wallpapers system-wide? [y/n] " yn
	case $yn in
		Y | y ) SYSTEM_WIDE=1; break;;
		N | n ) break;;
		* ) echo "Please respond with with \"y\" or \"n\". Case insensitive.";;
	esac
done

set_up

raise_sudo mkdir -p $WALLPAPERS_DIRECTORY
raise_sudo cp -r $SCRIPT_DIR/images/* $WALLPAPERS_DIRECTORY

if [[ ! -z $BACKGROUND_PROPERTIES ]]; then
	raise_sudo mkdir -p $BACKGROUND_PROPERTIES
	raise_sudo cp $SCRIPT_DIR/configs/* $BACKGROUND_PROPERTIES

	for i in $BACKGROUND_PROPERTIES/*; do
		raise_sudo sed -i "s|REPLACE_ME|${WALLPAPERS_DIRECTORY}|g" $i
	done
	echo "Installed properties to ${BACKGROUND_PROPERTIES}."
fi

echo "Installed wallpapers to ${WALLPAPERS_DIRECTORY}."
