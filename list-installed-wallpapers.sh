#/bin/bash
shopt -s globstar
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

source $SCRIPT_DIR/folders.sh

wallpaper_existence(){
	printf "Wallpapers present in $1...  "
	for i in $SCRIPT_DIR/images/**/*.png; do
		name=`basename $i`
		if [ -f $1/**/$name ]; then
			printf "\x1B[32mYES\x1B[0m\n"
			return
		fi
	done
	printf "\x1B[31mNO\x1B[0m\n"
}

properties_existence(){
	printf "Properties present in $1...  "
	for i in $SCRIPT_DIR/configs/**/*.xml.in; do
		name=`basename $i`
		if [ -f $1/**/$name ]; then
			printf "\x1B[32mYES\x1B[0m\n"
			return
		fi
	done
	printf "\x1B[31mNO\x1B[0m\n"
}

echo -e "\e[31m\e[1mWallpapers files\e[0m:"
wallpaper_existence $USER_WP_DIR
wallpaper_existence $ROOT_WP_DIR

echo -e "\n\e[31m\e[1mConfig files for GNOME\e[0m:"
properties_existence $USER_BG_CFG
properties_existence $ROOT_BG_CFG

echo -e "\n\e[31mFor safety reasons, please remove desired folders manually.\e[0m"
